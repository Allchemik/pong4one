﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour {

    private float speed;
    public float startSpeed;
    Rigidbody2D _rb2d;
    
	// Use this for initialization
	void Awake () {
        _rb2d = GetComponent<Rigidbody2D>();
        speed = startSpeed;
        startRandomMovement();

    }
	
    public void speedUp()
    {
        speed = speed + startSpeed * 0.01f;
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        speedUp();
        _rb2d.velocity = _rb2d.velocity.normalized * speed;
    }
    public void startRandomMovement()
    {
        _rb2d.velocity = new Vector2(Random.Range(-0.5f, -0.2f), Random.Range(-0.5f, 0.5f)).normalized * speed;
    }
}
