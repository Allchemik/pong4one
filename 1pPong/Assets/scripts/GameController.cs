﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour {

    public static GameController Instance = null;

    public GameObject ball;

    public Text gameOverText;
    public Text scoreText;
    public Text highScoreText;
    public Text punktyText;
    public Text countDown;
    public Text continueText;
    public Image fade;


    public Image[] lifeimages;
    public int lifes;
    private int Timeout;

    private int punkty;
    private bool newRecord = false;
    private bool paused;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }

        gameOverText.enabled = false;
        scoreText.enabled = false;
        highScoreText.enabled = false;
        fade.enabled = false;
        continueText.enabled = false;

        punkty = 0;
        punktyText.enabled = true;
        punktyText.text = punkty.ToString();

        for (int i=0;i<lifeimages.Length;++i)
        {
            lifeimages[i].enabled = true;
        }

        newLive();
    }

    private void GameOver()
    {
        fade.enabled = true;
        continueText.enabled = true;
        gameOverText.enabled = true;
        paused = true;
        if (newRecord)
        {
            scoreText.text = "score: " + punkty.ToString() +"\n New Record!";
        }
        else
        {
            scoreText.text = "score: " + punkty.ToString();
        }
        scoreText.enabled = true;
        highScoreText.text = "highscore: "+ PlayerPrefs.GetInt("Highscore").ToString();
        highScoreText.enabled = true;

        punktyText.enabled = false;
    }

    public void AddPoints()
    {
        punkty++;
        UpdatePoints();
    }

    public void UpdatePoints()
    {
        punktyText.text = punkty.ToString();
        if (punkty > PlayerPrefs.GetInt("Highscore"))
        {
            newRecord = true;
            PlayerPrefs.SetInt("Highscore", punkty);
        }
    }

    public void newLive()
    {
        if(--lifes>=0)
        {
            paused = false;
            lifeimages[lifes].enabled = false;
            Timeout = 3;
            StartCoroutine(CountDown());
        }
        else
        {
            GameOver();
        }
    }
    public void backToMenu()
    {
        SceneManager.LoadScene(0);
    }

    IEnumerator CountDown()
    {
        while (Timeout>0)
        {
            countDown.enabled = true;
            countDown.text = Timeout.ToString();
            yield return new WaitForSeconds(1f);
            Timeout--;
        }
        countDown.enabled = false;
        GameObject temp = Instantiate(ball, Vector3.zero, Quaternion.identity) as GameObject;
    }
    private void Update()
    {
        if (Input.GetButtonDown("Cancel"))
        {
            if (!paused)
            {
                paused = true;
                countDown.enabled = true;
                countDown.text = "Pause";
                continueText.enabled = true;
                Time.timeScale = 0f;

            }
            else
            {
                countDown.enabled = false;
                continueText.enabled = false;
                paused = false;
                Time.timeScale = 1f;
            }
            return;
        }
        if (Input.GetButtonDown("Submit"))
        {
            if (paused)
            {
                backToMenu();
            }
        }
    }
}
