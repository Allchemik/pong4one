﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class bounds
{
    public float min;
    public float max;
}

public class Player : MonoBehaviour {

    public float speed;
    public bounds bound;
    Rigidbody2D _rb2d;

	// Use this for initialization
	void Start () {
        _rb2d = GetComponent<Rigidbody2D>();
    }
	
	// Update is called once per frame
	void Update () {
        float v = Input.GetAxisRaw("Vertical");
        if(v>0.1f)
        {
            if (transform.position.y < bound.max)
            {
                _rb2d.velocity = new Vector2(0f, speed);
            }
            else
            {
                _rb2d.velocity = new Vector2(0f, 0f);
            }
        }
        else if(v<-0.1f)
        {
            if (transform.position.y > bound.min)
            {
                _rb2d.velocity = new Vector2(0f, -speed);
            }
            else
            {
                _rb2d.velocity = new Vector2(0f, 0f);
            }
        }
        else
        {
            _rb2d.velocity = new Vector2(0f, 0f);
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.CompareTag("Ball"))
        {
            GameController.Instance.AddPoints();
        }
    }
}
