﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class wallController : MonoBehaviour {

    public bool alreadyTouched;

    void OnCollisionEnter2D(Collision2D collision)
    {
        alreadyTouched = true;
    }
    void OnCollisionExit2D(Collision2D collision)
    {
        alreadyTouched = false;
    }
    private void OnCollisionStay2D(Collision2D collision)
    {
        if(collision.gameObject.CompareTag("Ball"))
        {
            collision.gameObject.GetComponent<Ball>().startRandomMovement();
        }
    }
}
